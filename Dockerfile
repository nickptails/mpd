ARG ALPINE_VERSION

FROM alpine:${ALPINE_VERSION} AS builder

ARG ALPINE_VERSION
ARG MPD_VERSION
ARG MPD_VERSION_MICRO

RUN apk add --no-cache \
        alsa-lib-dev \
        avahi-dev \
        boost-dev \
        build-base \
        curl \
        curl-dev \
        expat-dev \
        faad2-dev \
        ffmpeg4-dev \
        flac-dev \
        fmt-dev \
        glib-dev \
        gtest-dev \
        icu-dev \
        jack-dev \
        lame-dev \
        libao-dev \
        libcdio-paranoia-dev \
        libid3tag-dev \
        libmad-dev \
        libmodplug-dev \
        libmpdclient-dev \
        libnfs-dev \
        libogg-dev \
        libsamplerate-dev \
        libshout-dev \
        libvorbis-dev \
        meson \
        opus-dev \
        pipewire-dev \
        pulseaudio-dev \
        py3-attrs \
        py3-sphinx \
        samba-dev \
        soxr-dev \
        wavpack-dev && \
    curl -fLo mpd-${MPD_VERSION}.${MPD_VERSION_MICRO}.tar.xz \
        "https://www.musicpd.org/download/mpd/${MPD_VERSION}/mpd-${MPD_VERSION}.${MPD_VERSION_MICRO}.tar.xz" && \
    tar xJf mpd-${MPD_VERSION}.${MPD_VERSION_MICRO}.tar.xz && \
    cd mpd-${MPD_VERSION}.${MPD_VERSION_MICRO} && \
    curl -fLo libcdio-paranoia-version.patch \
        "https://git.alpinelinux.org/aports/plain/community/mpd/libcdio-paranoia-version.patch?h=${ALPINE_VERSION}-stable" && \
    curl -fLo stacksize.patch \
        "https://git.alpinelinux.org/aports/plain/community/mpd/stacksize.patch?h=${ALPINE_VERSION}-stable" && \
    patch -p1 < libcdio-paranoia-version.patch && \
    patch -p1 < stacksize.patch && \
    mkdir -p build && \
    meson build \
        -Dshout=enabled \
        -Dopus=enabled \
        -Dmodplug=enabled \
        -Dnfs=enabled \
        -Dsmbclient=enabled \
        -Dffmpeg=enabled \
        -Dlibmpdclient=enabled \
        -Dcdio_paranoia=enabled \
        -Dzeroconf=avahi \
        -Dtest=false \
        -Ddocumentation=disabled \
        -Dwavpack=enabled \
        -Dpipewire=enabled \
        -Dsnapcast=true && \
    DESTDIR=/mpd ninja -C build install && cd - && \
    curl -JOL https://github.com/MusicPlayerDaemon/MPD/raw/master/doc/mpdconf.example && \
    mkdir -p /mpd/etc && \
    sed	-e 's:\#user.*:user\t\t"mpd":' \
        -e 's:\#log_file.*:log_file\t\t"syslog":' \
        mpdconf.example > /mpd/etc/mpd.conf


FROM alpine:${ALPINE_VERSION}

COPY --from=builder /mpd/usr/local/bin/ /usr/local/bin/
COPY --from=builder /mpd/etc/ /etc/
RUN apk add --no-cache \
        alsa-lib \
        avahi-libs \
        busybox \
        expat \
        faad2-libs \
        ffmpeg4-libs \
        flac \
        fmt \
        icu-libs \
        lame \
        libao \
        libbz2 \
        libcdio \
        libcdio-paranoia \
        libcurl \
        libgcc \
        libid3tag \
        libmad \
        libmodplug \
        libmpdclient \
        libnfs \
        libogg \
        libpulse \
        libsamplerate \
        libshout \
        libsmbclient \
        libstdc++ \
        libvorbis \
        musl \
        opus \
        pipewire-jack \
        pipewire-libs \
        soxr \
        wavpack \
        zlib && \
    mkdir -p \
        /var/lib/mpd/.cache \
        /var/lib/mpd/playlists \
        /var/lib/mpd/music \
        /var/log/mpd \
        /var/run/mpd && \
    adduser -S -D -h /var/lib/mpd -s /sbin/nologin -G audio -g mpd mpd 2>/dev/null && \
    chown mpd:audio \
        /var/lib/mpd/.cache \
        /var/lib/mpd/playlists \
        /var/lib/mpd/music \
        /var/log/mpd \
        /var/run/mpd

USER mpd

CMD ["mpd"]
